<?php
session_start();
if(!isset($_SESSION["username"])) {
header('Location:index.html'); }
 error_reporting(0);
require_once("dbcontroller.php");
$db_handle = new DBController();
//header("Refresh:5; url=index.php");
?>

<!DOCTYPE HTML>
<html>
<head>
<title>Book Shopping</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700' rel='stylesheet' type='text/css'>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<script src="js/jquery.min.js"></script> 
<!-- start top_js_button -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript" src="js/cart.js"></script>
   <script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
			});
		});
	</script>
</head>
<body>
<!-- start header -->
<div class="header_bg">
<div class="wrap">
	<div class="header">
		<div class="logo">
                    <a href="index.php"><img style="margin-left: 2em; margin-bottom: 0.2em;" src="images/logo.png" width="50px" height="50px"/><div style="font-size: 2em;">Book Shop</div></a>
		</div>
		<div class="h_icon">
		<ul class="icon1 sub-icon1">
                    <li><a class="active-icon c1" href="#"><i><?php if(isset($_SESSION['item_total'])) { echo $_SESSION['item_total']; } else { echo "0"; } ?></i></a>
				<ul class="sub-icon1 list">
                                    <a href="checkout.php"><li><h3>Checkout</h3></li></a>
                                    <a href="handler.php?action=empty"><li><h3>Clear</h3></li></a> 
                                    <a href="logout.php"><li><h3>Log Out</h3></li></a>    
				</ul>
			</li>
		</ul>
		</div>
		<div class="h_search">
    		<form>
    			<input type="text" value="">
    			<input type="submit" value="">
    		</form>
		</div>
		<div class="clear"></div>
	</div>
</div>
</div>
<div class="header_btm">
<div class="wrap">
	<div class="header_sub">
		<div class="h_menu">
			<ul>
                            <li class="active"><a href="index.php">Home</a></li> |
                            <li><a href="contact.php">Contact us</a></li>
			</ul>
		</div>
	<div class="clear"></div>
</div>
</div>
</div>
<!-- start main -->
<div class="main_bg">
<div class="wrap">	
	<div class="main">
            <h2 class="style top">BOOKS</h2><br/>        
		<!-- start grids_of_3 -->
                <iframe src="iframe.php?ref=iframe" id="ifr" class="ifr" width="1100" height="100%" marginheight="0" frameborder="0" onLoad="autoResize('ifr');"></iframe>
                <div style="width: 100%">
		<div class="grids_of_3">
                    <?php
                    $product_array = $db_handle->runQuery("SELECT * FROM tblproduct ORDER BY id ASC");
                    if (!empty($product_array)) { 
                    foreach($product_array as $key=>$value){
                    ?>
			<div class="grid1_of_3">
                            <form method="post" action="handler.php?action=add&code=<?php echo $product_array[$key]["code"]; ?>">
                                <a href="#"><img src="<?php echo $product_array[$key]["image"]; ?>" alt="" width="145" height="200"/></a>
                            <h3><?php echo $product_array[$key]["name"]; ?></h3>
                            <div style="color: grey"><input min="0" max="9" size="2" type="number" name="quantity" value="0" maxlength="2" style="width: 28px; color: white; background: #77CA9E;"/></div><br/>
                            <div class="price">                               
                                <h4>&#8377 <?php echo $product_array[$key]["price"]; ?></h4> 
                           <div class="contact-form"><span><input type="submit" class="" value="Add to cart"></span></div>
                            </div>
                            <span class="b_btm"></span>
                            </form>
			</div> 
                    <?php } } ?>          
		</div>
                    <!-- <iframe src="iframe.php?id=1" height="1410" width="600"></iframe> -->
                </div>     
		<div class="clear"></div>
		<!-- end grids_of_3 -->
	</div>
</div>
</div>		

<!-- start footer -->
<div class="footer_bg1" style="height: 70px;">
<div class="wrap">
	<div class="footer">
		<!-- scroll_top_btn -->
	    <script type="text/javascript">
			$(document).ready(function() {
			
				var defaults = {
		  			containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
		 		};
				
				
				$().UItoTop({ easingType: 'easeOutQuart' });
				
			});
		</script>
                <script language="JavaScript">
                function autoResize(id){
                   var newheight;
                    var newwidth;

                    if(document.getElementById){
                    newheight=document.getElementById(id).contentWindow.document.body.scrollHeight;
                    newwidth=document.getElementById(id).contentWindow.document.body.scrollWidth;
                    }

    document.getElementById(id).height= (newheight) + "px";
    document.getElementById(id).width= (newwidth) + "px";
}
//-->
</script>
		 <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
		<!--end scroll_top_btn -->
		<div class="copy">
			<p>Developed by Dhruvdutt Jadhav</p>
		</div>
		<div class="clear"></div>
	</div>
</div>
</div>
</body>
</html>