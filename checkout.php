<?php
session_start();
if(!isset($_SESSION["username"])) {
header('Location:index.html'); }
require_once("dbcontroller.php");
$db_handle = new DBController();
error_reporting(0);
?>

<!DOCTYPE HTML>
<html>
<head>
<title>Book Shopping</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700' rel='stylesheet' type='text/css'>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/viewrecords.css" rel="stylesheet" type="text/css" media="all" />
<script src="js/jquery.min.js"></script> 
<!-- start top_js_button -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript" src="js/cart.js"></script>
   <script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
			});
		});
	</script>
</head>
<body>
<!-- start header -->
<div class="header_bg">
<div class="wrap">
	<div class="header">
		<div class="logo">
                    <a href="index.php"><img style="margin-left: 2em; margin-bottom: 0.2em;" src="images/logo.png" width="50px" height="50px"/><div style="font-size: 2em;">Book Shop</div></a>
		</div>
		<div class="h_icon">
		<ul class="icon1 sub-icon1">
                    <li><a class="active-icon c1" href="#"><i><?php if(isset($_SESSION['item_total'])) { echo $_SESSION['item_total']; } else { echo "0"; } ?></i></a>
				<ul class="sub-icon1 list">
                                    <a href="checkout.php"><li><h3>Checkout</h3></li></a>
                                    <a href="handler.php?action=empty"><li><h3>Clear</h3></li></a>  
                                    <a href="logout.php"><li><h3>Log Out</h3></li></a>
				</ul>
			</li>
		</ul>
		</div>
		<div class="h_search">
    		<form>
    			<input type="text" value="">
    			<input type="submit" value="">
    		</form>
		</div>
		<div class="clear"></div>
	</div>
</div>
</div>
<div class="header_btm">
<div class="wrap">
	<div class="header_sub">
		<div class="h_menu">
			<ul>
                            <li><a href="index.php">Home</a></li> |
                            <li><a href="contact.php">Contact us</a></li>
			</ul>
		</div>
	<div class="clear"></div>
</div>
</div>
</div>
<!-- start main -->
<div class="main_bg">
<div class="wrap">	
<div class="main">
	 	 <div class="contact">
				  <div class="contact-form">
			 	  	 	<h2>Your Cart</h2><br/>
                                  </div>
                 </div>
</div>
    <div class="clear"></div>
    <table style="margin-left: 1em;">
<tbody>
<tr>
<th>Name</th>
<th>Code</th>
<th>Quantity</th>
<th>Price</th>
<th>Sub Total</th>
<th>Action</th>
</tr>	
<?php
    foreach ($_SESSION["cart_item"] as $item){
		?>
				<tr>
				<td><strong><?php echo $item["name"]; ?></strong></td>
				<td><?php echo $item["code"]; ?></td>
				<td><?php echo $item["quantity"]; ?></td>
				<td align=right><?php echo "&#8377 ".$item["price"]; ?></td>
                                <td align=right><?php echo "&#8377 ".$item["price"]*$item["quantity"].".00"; ?></td>
                                <td><a href="handler.php?action=remove&code=<?php echo $item["code"]; ?>" class="btnRemoveAction">Remove Item</a></td>
				</tr>
				<?php
        $item_total += ($item["price"]*$item["quantity"]);
		}
                $_SESSION['item_total']=$item_total;
                //header("Refresh:0");
                header("Refresh:5; url=checkout.php");
                ?>
</tbody>
    </table><br/><hr style="color: gainsboro;"/>
    <p style="margin-left: 39.5%; font-size: 25px;"><?php echo "Total : &nbsp;&nbsp;&nbsp;&nbsp;  &#8377 ".$item_total.".00"; ?></p><br/>
    <div style="margin-left:39.5%">
        <div class="contact-form"><a href="form.php"><input type="submit" class="" value="Proceed"></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="handler.php?action=empty"><input type="submit" class="" value="Cancel"></a></div>
    </div>
<!-- start footer -->
<br/><br/>
<div class="footer_bg1" style="height: 70px;">
<div class="wrap">
	<div class="footer">
		<!-- scroll_top_btn -->
	    <script type="text/javascript">
			$(document).ready(function() {
			
				var defaults = {
		  			containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
		 		};
				
				
				$().UItoTop({ easingType: 'easeOutQuart' });
				
			});
		</script>
		 <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
		<!--end scroll_top_btn -->
		<div class="copy">
			<p>Developed by Dhruvdutt Jadhav</p>
		</div>
		<div class="clear"></div>
	</div>
</div>
</div>
</body>
</html>