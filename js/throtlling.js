        function validate(e) {

            //e = e || window.event;
            //var myField = e.target || e.src;
            var myField = e;
            var xmlhttp;
            
            //alert("e="+e);
            //alert("myfield="+myField);

            if (window.XMLHttpRequest)
            {
                xmlhttp = new XMLHttpRequest();
            }
            else
            {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }

            xmlhttp.onreadystatechange = function()
            {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                {
                    var msg=xmlhttp.responseText;
                    var response = msg.split("||");
                    
                    var divname = "err"+myField.id.substring(3);
                    var mydiv = document.getElementById(divname);
                    
                    if(!eval(response[0]))
                    {
                        mydiv.innerHTML = response[1];
                        myField.valid = false;
                    }
                    else
                    {
                        myField.valid = true;
                        mydiv.innerHTML = "";
                    }

            var mybutton = document.getElementById("btnRegister");
            mybutton.style.backgroundColor="grey";
            mybutton.disabled=!isValidForm();
            if(!mybutton.disabled)
            {
                mybutton.style.backgroundColor="#3CC395";
            }
                }
            }
            //alert("myfield id="+myField.id);
            //alert("myfield value="+myField.value);
            xmlhttp.open("GET","validatedata.php?"+myField.id+"="+myField.value,true);
            xmlhttp.send();
        }

        function isValidForm()
        {
            var f1 = document.getElementById("txtFirstName");
            var f2 = document.getElementById("txtLastName");
            var f3 = document.getElementById("txtMobile");
            var f4 = document.getElementById("txtEmail");
            var f5 = document.getElementById("txtAddress");
            var f6 = document.getElementById("txtCity");
            var f7 = document.getElementById("txtState");
            var f8 = document.getElementById("txtPincode");
            
            return (f1.valid && f2.valid && f3.valid && f4.valid && f5.valid&& f6.valid && f7.valid && f8.valid);
        }