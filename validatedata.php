<?php
session_start();
if(!isset($_SESSION["username"])) {
header('Location:index.html'); }
 error_reporting(0);
$v = false;
$m = "An error has occured";

if(isset ($_GET["txtFirstName"]))
{
    if($_GET["txtFirstName"] == "")
    {
        $v = false;
        $m = "*Required";
    }
    
    else
    {
        $v = true;
        $m = "";
    }
}

else if(isset ($_GET["txtLastName"]))
{
    if($_GET["txtLastName"] == "")
    {
        $v = false;
        $m = "*Required";
    }
    
    else
    {
        $v = true;
        $m = "";
    }
}
    
else if(isset ($_GET["txtMobile"]))
{   
    $expr="/^\d{10}$/";
    if($_GET["txtMobile"]=="")
    {
        $v = false;
        $m = "*Required";
    }
    else if(!preg_match($expr, $_GET["txtMobile"]))    {
        $v = false;
        $m = "*Numeric Values Only";
    }
    else
    {
        $v = true;
        $m = "t";
    }
}

else if(isset ($_GET['txtEmail']))
{
    if($_GET["txtEmail"]=="")
    {
        $v = false;
        $m = "*Required";
    }
    else if(filter_var($_GET["txtEmail"],FILTER_VALIDATE_EMAIL))
    {
        $v = true;
        $m = "";
    }
    else
    {
        $v = false;
        $m = "*Invalid";
    }
}

else if(isset ($_GET["txtAddress"]))
{
    if($_GET["txtAddress"] == "")
    {
        $v = false;
        $m = "*Required";
    }
    
    else
    {
        $v = true;
        $m = "";
    }
}

else if(isset ($_GET["txtCity"]))
{
    if($_GET["txtCity"] == "")
    {
        $v = false;
        $m = "*Required";
    }
    
    else
    {
        $v = true;
        $m = "";
    }
}

else if(isset ($_GET["txtState"]))
{
    if($_GET["txtState"] == "")
    {
        $v = false;
        $m = "*Required";
    }
    
    else
    {
        $v = true;
        $m = "";
    }
}

else if(isset ($_GET["txtPincode"]))
{
    $expr="/^\d{6}$/";
    if($_GET["txtPincode"]=="")
    {
        $v = false;
        $m = "*Required";
    }
    else if(!preg_match($expr, $_GET["txtPincode"]))    {
        $v = false;
        $m = "*Invalid";
    }
    else
    {
        $v = true;
        $m = "t";
    }
}

else
{
    $v = true;
    $m = "";
    $output = "true||".$m;
}
echo $v."||".$m;
?>
