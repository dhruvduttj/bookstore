<?php
session_start();
if(!isset($_SESSION["username"])) {
header('Location:index.html'); }
 error_reporting(0);
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Book Shopping</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700' rel='stylesheet' type='text/css'>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<script src="js/jquery.min.js"></script> 
<!-- start top_js_button -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript" src="js/cart.js"></script>
<script type="text/javascript" src="js/throtlling.js"></script>
   <script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
			});
		});
	</script>
</head>
<body>
<div class="header_bg">
<div class="wrap">
	<div class="header">
		<div class="logo">
                    <a href="index.php"><img style="margin-left: 2em; margin-bottom: 0.2em;" src="images/logo.png" width="50px" height="50px"/><div style="font-size: 2em;">Book Shop</div></a>
		</div>
		<div class="h_icon">
		<ul class="icon1 sub-icon1">
                    <li><a class="active-icon c1" href="#"><i><?php if(isset($_SESSION['item_total'])) { echo $_SESSION['item_total']; } else { echo "0"; } ?></i></a>
				<ul class="sub-icon1 list">
                                    <a href="checkout.php"><li><h3>Checkout</h3></li></a>
                                    <a href="handler.php?action=empty"><li><h3>Clear</h3></li></a>  
                                    <a href="logout.php"><li><h3>Log Out</h3></li></a>    
				</ul>
			</li>
		</ul>
		</div>
		<div class="h_search">
    		<form>
    			<input type="text" value="">
    			<input type="submit" value="">
    		</form>
		</div>
		<div class="clear"></div>
	</div>
</div>
</div>
<div class="header_btm">
<div class="wrap">
	<div class="header_sub">
		<div class="h_menu">
			<ul>
                            <li><a href="index.php">Home</a></li> |
                            <li class="active"><a href="contact.php">Contact us</a></li>
			</ul>
		</div>
	<div class="clear"></div>
</div>
</div>
</div>
<!-- start main -->
<div class="main_bg">
<div class="wrap">	
<div class="main">
	 	 <div class="contact">
				  <div class="contact-form">
			 	  	 	<h2>shipping details</h2><br/>                                                          
<form action="handler.php?action=empty" method="post">
<table border="1" cellpadding="6px">
<tr>
<td align="right">First Name : &nbsp;&nbsp;&nbsp;</td>
<td style="padding-right: 30px;"><input width="150%" type="text" id="txtFirstName" name="txtFirstName" onchange="validate(this)" onblur="validate(this)"/></td>
<td><div style="color: grey;" id="errFirstName"/></td>
</tr
<tr>
<td align="right">Last Name : &nbsp;&nbsp;&nbsp;</td>
<td style="padding-right: 30px;"><input type="text" id="txtLastName" name="txtLastName" onchange="validate(this)" onblur="validate(this)"/></td>
<td><div style="color: grey;" id="errLastName"/></td>
</tr>
<tr>
<td align="right">Mobile : &nbsp;&nbsp;&nbsp;</td>
<td style="padding-right: 30px;"><input maxlength="10" type="text" id="txtMobile" name="txtMobile" onchange="validate(this)" onblur="validate(this)"/></td>
<td><div style="color: grey;" id="errMobile"/></td>
</tr>
<tr>
<td align="right">Email : &nbsp;&nbsp;&nbsp;</td>
<td style="padding-right: 30px;"><input type="text" id="txtEmail" name="txtEmail" onchange="validate(this)" onblur="validate(this)"/></td>
<td><div style="color: grey;" id="errEmail"/></td>
</tr>
<tr>
<td align="right">Address : &nbsp;&nbsp;&nbsp;</td>
<td style="padding-right: 30px;"><input type="text" id="txtAddress" name="txtAddress" onchange="validate(this)" onblur="validate(this)" /></td>
<td><div style="color: grey;" id="errAddress"/></td>
</tr>
<tr>
<td align="right">City : &nbsp;&nbsp;&nbsp;</td>
<td style="padding-right: 30px;"><input type="text" id="txtCity" name="txtCity" onchange="validate(this)" onblur="validate(this)" /></td>
<td><div style="color: grey;" id="errCity"/></td>
</tr>
<tr>
<td align="right">State : &nbsp;&nbsp;&nbsp;</td>
<td style="padding-right: 30px;"><input type="text" id="txtState" name="txtState" onchange="validate(this)" onblur="validate(this)" /></td>
<td><div style="color: grey;" id="errState"/></td>
</tr>
<tr>
<td align="right">Pincode : &nbsp;&nbsp;&nbsp;</td>
<td style="padding-right: 30px;"><input maxlength="6" type="text" id="txtPincode" name="txtPincode" onchange="validate(this)" onblur="validate(this)" /></td>
<td><div style="color: grey;" id="errPincode"/></td>
</tr>
<tr><td></td><td align="center">
        <input type="submit" id="btnRegister" value="Submit" disabled="true" style="background-color: grey;"/>
</td></tr>
</table></form>
                                                
                                                
                                                
			 	 	  
				    </div>
  				<div class="clear"></div>		
			  </div>
		</div>
</div>
</div>		
<!-- start footer -->
<div class="footer_bg1" style="height: 70px;">
<div class="wrap">
	<div class="footer">
		<!-- scroll_top_btn -->
	    <script type="text/javascript">
			$(document).ready(function() {
			
				var defaults = {
		  			containerID: 'toTop', // fading element id
					containerHoverID: 'toTopHover', // fading element hover id
					scrollSpeed: 1200,
					easingType: 'linear' 
		 		};
				
				
				$().UItoTop({ easingType: 'easeOutQuart' });
				
			});
		</script>
		 <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>
		<!--end scroll_top_btn -->
		<div class="copy">
			<p>Developed by Dhruvdutt Jadhav</p>
		</div>
		<div class="clear"></div>
	</div>
</div>
</div>
</body>
</html>