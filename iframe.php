<?php
session_start();
if(!isset($_SESSION["username"])) {
header('Location:index.html'); }
require_once("dbcontroller.php");
$db_handle = new DBController();
error_reporting(0);
$ref=$_GET['ref'];
?>

<!DOCTYPE HTML>
<html>
<head>
<title>Iframe</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700' rel='stylesheet' type='text/css'>
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/viewrecords.css" rel="stylesheet" type="text/css" media="all" />
<script src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript" src="js/cart.js"></script>
</head>
<body>
<table>
<tbody>
<?php
    foreach ($_SESSION["cart_item"] as $item){
		?>
				<tr>
				<td><strong><?php echo $item["name"]; ?></strong></td>
				<td><?php echo $item["quantity"]; ?></td>
				<td align=right><?php echo "&#8377 ".$item["price"]; ?></td>
                                <td align=right><?php echo "&#8377 ".$item["price"]*$item["quantity"].".00"; ?></td>
				</tr>
    <?php }
                ?>
</tbody>
    </table>
</body>
</html>